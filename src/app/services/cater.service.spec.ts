import { TestBed } from '@angular/core/testing';

import { CaterService } from './cater.service';

describe('CaterService', () => {
  let service: CaterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
