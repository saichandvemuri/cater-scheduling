import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import Swal from 'sweetalert2';



@Injectable({
  providedIn: 'root'
})
export class CaterService {
  public baseURL;
  public ipAddress: any={ip:'  '};
    httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json'
      'Content-Type': 'text/plain'

    }),
  };

  constructor(
    private _http: HttpClient
  ) {
  }
  public geturl() {
    let data = localStorage.getItem('webserviceURL');
    this.baseURL = data;
    let address = JSON.parse(sessionStorage.getItem('ipAdd'));
    if(address!=undefined){
      this.ipAddress=address;
    }else{
      console.log(this.ipAddress)
      sessionStorage.setItem("ipAdd", JSON.stringify(this.ipAddress))
    }
    // console.log("ip",this.ipAddress)
  }
  public authenticateUser(userID, password, catererId) {
    this.geturl();
    let obj = {
      "userId": userID,
      "password": password,
      "catererId": catererId,
      "deviceId": this.ipAddress.ip
    }
    return this._http.post <object>(this.baseURL + "authenticateUser",obj,this.httpOptions).pipe(catchError(this.errorHandler));
  }

  public getEventsList(catererId, cisnumber, startdate, endDate, userId, sessionId, deviceId) {
    this.geturl();
    return this._http.get(this.baseURL + 'getEventList?catererId=' + catererId + '&cisNumber=' + cisnumber + '&eventStartDate=' + startdate + '&eventEndDate=' + endDate + '&userId=' + userId + '&deviceId=' + deviceId + '&platform=WebSignInAPP&sessionId=' + sessionId).pipe(catchError(this.errorHandler));
  }
  public getEventList(catererId, cisnumber, startdate, endDate, userId, sessionId) {
    this.geturl();
    return this._http.get(this.baseURL + 'getEventList?catererId=' + catererId + '&cisNumber=' + cisnumber + '&eventStartDate=' + startdate + '&eventEndDate=' + endDate + '&userId=' + userId + '&deviceId=' + this.ipAddress.ip + '&platform=WebSignInAPP&sessionId=' + sessionId).pipe(catchError(this.errorHandler));
  }
  public getEventStaffingInfo(caterId, eventId) {
    this.geturl();
    return this._http.get(this.baseURL + 'getEventStaffingInfo?catererId=' + caterId + '&eventId=' + eventId).pipe(catchError(this.errorHandler));
  }
  public getRosterData(catererId, eventId, sessionId, workerName, mode, positionTypeId,eventDate) {
    this.geturl();
    return this._http.get(this.baseURL + 'getRosterData?catererId=' + catererId + '&eventId=' + eventId + '&sessionId=' + sessionId + '&workerName=' + workerName + '&mode=' + mode + '&positionTypeId=' + positionTypeId+'&eventDate='+eventDate).pipe(catchError(this.errorHandler));
  }
  public saveToSignInStaging(params) {
    this.geturl();
    return this._http.post(this.baseURL + 'saveToSignInStaging', params, this.httpOptions).pipe(catchError(this.errorHandler))
  }
  public getUnscheduledWorkersList(catrerid, workername, userId, sessionId, eventId, positionTypeId,eventDate) {
    this.geturl();
    return this._http.get(this.baseURL + 'getUnscheduledWorkersList?catererId=' + catrerid + '&workerName=' + workername + '&userId=' + userId + '&deviceId=' + this.ipAddress.ip + '&platform=WebSignInAPP&sessionId=' + sessionId + '&eventId=' + eventId + '&positionTypeId=' + positionTypeId+'&eventDate='+eventDate).pipe(catchError(this.errorHandler));
  }

  public approveUnscheduledWorker(params):Observable<any> {
    this.geturl();
    return this._http.post(this.baseURL + 'approveUnscheduledWorker', params, this.httpOptions).pipe(catchError(this.errorHandler));
  }

  public deleteUnscheduledWorker(catererId, stagingId, userId, sessionId) {
    this.geturl();
    return this._http.get(this.baseURL + 'deleteUnscheduledWorker?catererId=' + catererId + '&stagingId=' + stagingId + '&userId=' + userId + '&deviceId=' + this.ipAddress.ip + '&platform=WebSignInAPP&sessionId=' + sessionId).pipe(catchError(this.errorHandler))
  }


  public getIpAddress() {
    return this._http.get('https://api64.ipify.org/?format=json').pipe(map(res => {
      console.log(res)
      if (res) {
        sessionStorage.setItem("ipAdd", JSON.stringify(res))
      }
    },err=>{
      catchError(this.errorHandler)
    })).toPromise();
  }
  public getIpAddresss() {
    return this._http.get('https://api64.ipify.org/?format=json').pipe(catchError(this.errorHandler))

  }
  public getWorkerBreakList(catererId, stagingId) {
    this.geturl();
    return this._http.get(this.baseURL + 'getWorkerBreakList?catererId=' + catererId + '&stagingId=' + stagingId).pipe(catchError(this.errorHandler))
  }
  public getSummaryList(catererId, eventId, sessionId, workerName, positionTypeId,eventDate) {
    this.geturl();
    return this._http.get(this.baseURL + 'getSummaryList?catererId=' + catererId + '&eventId=' + eventId + '&sessionId=' + sessionId + '&workerName=' + workerName + '&positionTypeId=' + positionTypeId+'&eventDate='+eventDate).pipe(catchError(this.errorHandler));
  }
  public saveSummaryList(params) {
    this.geturl();
    return this._http.post(this.baseURL + 'saveSummaryList', params, this.httpOptions).pipe(catchError(this.errorHandler));
  }
  public logOutUser(catererId, sessionId) {
    this.geturl();
    return this._http.get(this.baseURL + 'logOutUser?catererId=' + catererId + '&sessionId=' + sessionId).pipe(catchError(this.errorHandler));
  }
  public getEventQRCode(catererId, eventId,eventDate) {
    this.geturl();
    return this._http.get(this.baseURL + '/getEventQRCode?catererId=' + catererId + '&eventId=' + eventId+'&eventDate='+eventDate).pipe(catchError(this.errorHandler));
  }
  public getAgencyWorkers(catererId, resourceId) {
    this.geturl();
    return this._http.get(this.baseURL + '/getAgencyWorkers?catererId=' + catererId + '&resourceId=' + resourceId).pipe(catchError(this.errorHandler));
  }
  public getFilterPreferences(catererId, userId):Observable<any> {
    this.geturl();
    return this._http.get(this.baseURL + '/getFilterPreferences?catererId=' + catererId + '&userId=' + userId).pipe(catchError(this.errorHandler));
  }
  public saveUnscheduledWorkerToStaging(params) {
    this.geturl();
    return this._http.post(this.baseURL + 'saveUnscheduledWorkerToStaging', params, this.httpOptions).pipe(catchError(this.errorHandler));
  }
  public geteventDetails() {
    let eventDetails = JSON.parse(sessionStorage.getItem('eventdetails'));
    return eventDetails
  }

  private errorHandler(error: HttpErrorResponse) {
    console.log("error in API service", error);
    Swal.fire({
      html:error.error,
      icon:'error'
    })
    return throwError(error);
  }

  public loggedIn(): boolean {
    return !!sessionStorage.getItem('userdetails');
  }
}
