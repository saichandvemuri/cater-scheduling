import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/login/login.component';
import { ListingComponent } from './core/listing/listing.component';
import { RosterEventComponent } from './core/roster-event/roster-event.component';
import { BreakSummaryComponent } from './core/break-summary/break-summary.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { BreakDetailsComponent } from './core/break-details/break-details.component';
import { DatePipe, APP_BASE_HREF, TitleCasePipe } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpInterceptorService } from './services/http-interceptor.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HeaderComponent } from './core/header/header.component';
import { DomsanitizePipe, HoursDifference } from './pipes/domsanitize.pipe';
import { EventComponent } from './core/mobile screens/event/event.component';
import { RoasterComponent } from './core/mobile screens/roaster/roaster.component';
import { SummaryMobileComponent } from './core/mobile screens/summary-mobile/summary-mobile.component';
import { BreaksummaryMobileComponent } from './core/mobile screens/breaksummary-mobile/breaksummary-mobile.component';
import { UnscheduledMobileComponent } from './core/mobile screens/unscheduled-mobile/unscheduled-mobile.component';
import { MobileHeaderComponent } from './core/mobile screens/mobile-header/mobile-header.component';
import { QrcodeComponent } from './core/mobile screens/qrcode/qrcode.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EventInfoComponent } from './core/mobile screens/event-info/event-info.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListingComponent,
    RosterEventComponent,
    BreakSummaryComponent,
    BreakDetailsComponent,
    HeaderComponent,
    DomsanitizePipe,
    EventComponent,
    RoasterComponent,
    SummaryMobileComponent,
    BreaksummaryMobileComponent,
    UnscheduledMobileComponent,
    MobileHeaderComponent,
    QrcodeComponent,
    EventInfoComponent,
    HoursDifference
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      closeButton:true,
      timeOut: 2000,
      preventDuplicates: true,
    }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    NgxSpinnerModule,
    NgSelectModule,
    ModalModule.forRoot(),
    AutocompleteLibModule,
    TypeaheadModule.forRoot(),
    NgbModule,
    TooltipModule.forRoot()


  ],
  providers: [DatePipe,HoursDifference
    , {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },],
  bootstrap: [AppComponent],

})
export class AppModule { }
