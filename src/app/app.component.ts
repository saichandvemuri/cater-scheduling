import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CaterService } from './services/cater.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private _http: HttpClient, private caterService: CaterService,public deviceService:DeviceDetectorService
  ) {
    if(this.deviceService.isMobile()){

    }else{
      this.caterService.getIpAddress();

    }
  }
  ngOnInit() {
    try {
      this._http.get("assets/url.json").subscribe(
        response => {
          let responseData: any = response;
          let webserviceURL = responseData.webserviceURL;
          localStorage.setItem("webserviceURL", webserviceURL);
          this.caterService.geturl();
        }
      )
    }
    catch (error) {
      console.log(error);
    }
  }
}
