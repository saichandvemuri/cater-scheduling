import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'domsanitize'
})
export class DomsanitizePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(value: any): any {
    return this.sanitizer.bypassSecurityTrustUrl(value);
  }
}



@Pipe({
  name:'hoursDifference'
})
export class HoursDifference implements PipeTransform{
  transform(startTime: any, endTime) {
    // console.log(startTime,endTime);
   let startMinutes=convertDateToMins(startTime);
   let endMinutes=convertDateToMins(endTime);
  return Number(endMinutes-startMinutes);

  }
}
function convertDateToMins(dateTime){
  let seconds=Date.parse(dateTime);

  let minutes=seconds/60000;

    return Number(minutes)
}
