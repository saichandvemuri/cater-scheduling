import { Component, OnInit } from '@angular/core';
import { CaterService } from 'src/app/services/cater.service';

@Component({
  selector: 'app-event-info',
  templateUrl: './event-info.component.html',
  styleUrls: ['./event-info.component.scss']
})
export class EventInfoComponent implements OnInit {
public eventDetails:any;
  constructor(caterService:CaterService) {
    console.log("event info")
    this.eventDetails = JSON.parse(sessionStorage.getItem('eventdetails'));

   }

  ngOnInit(): void {
  }

}
