import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnscheduledMobileComponent } from './unscheduled-mobile.component';

describe('UnscheduledMobileComponent', () => {
  let component: UnscheduledMobileComponent;
  let fixture: ComponentFixture<UnscheduledMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnscheduledMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnscheduledMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
