import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { CaterService } from 'src/app/services/cater.service';
import Swal from 'sweetalert2';
import { BreaksummaryMobileComponent } from '../breaksummary-mobile/breaksummary-mobile.component';


@Component({
  selector: 'app-unscheduled-mobile',
  templateUrl: './unscheduled-mobile.component.html',
  styleUrls: ['./unscheduled-mobile.component.scss']
})
export class UnscheduledMobileComponent implements OnInit {
  @ViewChild('template') modal;

  public windowHeight = (window.innerHeight - 180) + 'px';
  public bsmodelRef: BsModalRef;

  public userdetails: any;
  public deviceId: any;
  public eventDetails: any;
  public eventID;
  public unscheduledWorkerList: Array<any> = [];
  public minutesArray: Array<number> = new Array<number>(60);
  public workerList: Array<any> = []
  public personnelTypesList: Array<any> = [];
  public workerNameUnscheduled;
  public positionTypeIdunScheduled;

  public popUpminutes = null;
  public popUpHours = null;
  public popUpMeridian = null;
  public popUpDate;
  public popupIndex: number;
  public popupType: string;
  public popupTitle: string = '';

public eventDate='';
  public currentTime: Date;


  constructor(private route: ActivatedRoute, private router: Router, private modalService: BsModalService, private toastr: ToastrService, public caterService: CaterService, public datepipe: DatePipe, private activatedRoute: ActivatedRoute,) {
    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'));
    this.deviceId = JSON.parse(sessionStorage.getItem('ipAdd'));
    this.eventDetails = JSON.parse(sessionStorage.getItem('eventdetails'));
    // this.eventDate=this.eventDetails.nonEventFlag==1?this.datepipe.transform(this.eventDetails.eventDate,'MM/dd/yyyy'):'';

    this.eventID = this.activatedRoute.snapshot.params.eventId;
    console.log(this.eventID)

  }

  ngOnInit(): void {
    this.getUnscheduledWorkersList();

  }
  public getUnscheduledWorkersList() {
    this.formError=false;
    let date = this.datepipe.transform(this.eventDate, 'MM/dd/yyyy');
    try {
      this.caterService.getUnscheduledWorkersList(this.userdetails.catererId, this.workerNameUnscheduled == null ? '' : this.workerNameUnscheduled, this.userdetails.userId, this.userdetails.sessionId, this.eventID, this.positionTypeIdunScheduled == null ? '' : this.positionTypeIdunScheduled,    this.eventDate=this.eventDetails.nonEventFlag==1 && date!=null?this.datepipe.transform(this.eventDetails.eventDate,'MM/dd/yyyy'):'').subscribe(
        res => {
          console.log(res)
          let data: any = res;
          this.workerList = data.workerList;
          this.unscheduledWorkerList = data.unscheduledWorkerList;
          this.personnelTypesList = data.personnelTypesList;
          console.log(this.personnelTypesList)
          let i = 0;
          this.unscheduledWorkerList.map((x) => {
            if (x.break1Flag == 0) {
              this.unscheduledWorkerList[i].breakvalue = "start"
              this.unscheduledWorkerList[i].breakTaken = 0;
            } else if (x.break1Flag == 1) {
              this.unscheduledWorkerList[i].breakvalue = "end"
              this.unscheduledWorkerList[i].breakTaken = 1;
            } else if (x.break1Flag == 2) {
              if (x.break2Flag == 0) {
                this.unscheduledWorkerList[i].breakvalue = "start"
                this.unscheduledWorkerList[i].breakTaken = 1;
              } else if (x.break2Flag == 1) {
                this.unscheduledWorkerList[i].breakvalue = "end"
                this.unscheduledWorkerList[i].breakTaken = 2;
              }
              else if (x.break2Flag == 2) {
                if (x.break3Flag == 0) {
                  this.unscheduledWorkerList[i].breakvalue = "start"
                  this.unscheduledWorkerList[i].breakTaken = 2;
                } else if (x.break3Flag == 1) {
                  this.unscheduledWorkerList[i].breakvalue = "end"
                  this.unscheduledWorkerList[i].breakTaken = 3;
                }
                else if (x.break3Flag == 2) {
                  this.unscheduledWorkerList[i].breakTaken = 3;
                  this.unscheduledWorkerList[i].breakvalue = "complete"
                }
              }
            }
            i++;
          })

        }
      )
    } catch (error) {

    }
  }
  public saveToSignInStagingUnscheduled(i, flag, popupFlag) {
    console.log(this.unscheduledWorkerList[i].actualCallIn)
    console.log(this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a'))
    let paramdatepopup = this.datepipe.transform(this.popUpDate, 'MM/dd/yyyy') + " " + this.popUpHours + ":" + this.popUpminutes + " " + this.popUpMeridian;
    let param = {
      "eventId": this.eventID,
      "resourceID": this.unscheduledWorkerList[i].resourceId,
      "scheduleID": 0,
      "scheduledFlag": 0,
      "actualCallIn": flag == "signin" ? popupFlag ? paramdatepopup : this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy hh:mm a') == null ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy hh:mm a'),
      "actualCallOut": flag == "signout" ? popupFlag ? paramdatepopup : this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].actCallOut == undefined ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a') == null ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a'),
      "break1ActIns": "",
      "break1ActOuts": "",
      "break2ActIns": "",
      "break2ActOuts": "",
      "break3ActIns": "",
      "break3ActOuts": "",
      "personnelTypeId": this.unscheduledWorkerList[i].personnelTypeId,
      "agencyWorkerId": this.unscheduledWorkerList[i].agencyWorkerId,
      "agencyWorker": this.unscheduledWorkerList[i].agencyWorker,
      "biometricflag": 0,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId,
      "catererId": this.userdetails.catererId,
      "stagingId": this.unscheduledWorkerList[i].stagingId

    }
    try {
      this.caterService.saveToSignInStaging(param).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // alert(data.result)
          Swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1500,
            showConfirmButton: false
          });
          popupFlag ? this.bsmodelRef.hide() : '';
          this.getUnscheduledWorkersList();
        },
        (error) => {
          console.log("error came", error)
          Swal.fire({
            title: 'Failed',
            text: 'Failed to save',
            icon: 'error',
            // timer: 1200,
            // showConfirmButton: true
          });
        }
      )
    } catch (error) {

    }
  }
  public signInFlagUnscheduled(template: TemplateRef<any>, i, flag) {
    this.currentTime = new Date();
    this.popupIndex = i;
    this.popupType = flag;
    this.popupTitle = this.unscheduledWorkerList[i].resourceName;

    let condition = flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut;
    console.log(condition)

    if (condition == undefined) {
      this.saveToSignInStagingUnscheduled(i, flag, false)
    }
    else {
      if (condition.trim().length > 0) {
        this.popUpminutes = +this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'mm');
        this.popUpHours = +this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'hh');
        this.popUpMeridian = this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'a');
        this.popUpDate = this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy')
        this.bsmodelRef = this.modalService.show(template, Object.assign({}, { class: 'modal-container modal-dialog-centered' }));

      } else {
        this.saveToSignInStagingUnscheduled(i, flag, false)
      }
    }

  }
  public unschedulebreak(breaktype, breaknum, i) {
    this.currentTime = new Date();
    let param = {
      "eventId": this.eventID,
      "resourceID": this.unscheduledWorkerList[i].resourceId,
      "scheduleID": 0,
      "scheduledFlag": 0,
      "actualCallIn": this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy hh:mm a'),
      "actualCallOut": this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a') == null ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a'),
      "break1ActIns": breaknum == 0 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break1ActIns !== undefined ? this.unscheduledWorkerList[i].break1ActIns : '',
      "break1ActOuts": breaknum == 1 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break1ActOuts !== undefined ? this.unscheduledWorkerList[i].break1ActOuts : '',
      "break2ActIns": breaknum == 1 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break2ActIns !== undefined ? this.unscheduledWorkerList[i].break2ActIns : '',
      "break2ActOuts": breaknum == 2 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break2ActOuts !== undefined ? this.unscheduledWorkerList[i].break2ActOuts : '',
      "break3ActIns": breaknum == 2 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break3ActIns !== undefined ? this.unscheduledWorkerList[i].break3ActIns : '',
      "break3ActOuts": breaknum == 3 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break3ActOuts !== undefined ? this.unscheduledWorkerList[i].break3ActOuts : '',
      "personnelTypeId": this.unscheduledWorkerList[i].personnelTypeId,
      "agencyWorkerId": this.unscheduledWorkerList[i].agencyWorkerId,
      "agencyWorker": this.unscheduledWorkerList[i].agencyWorker,
      "biometricflag": 0,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId,
      "catererId": this.userdetails.catererId,
      "stagingId": this.unscheduledWorkerList[i].stagingId

    }

    console.log(param)
    this.saveToSignInStagingFinal(param, false)
  }
  public saveToSignInStagingFinal(param, flag) {
    try {
      this.caterService.saveToSignInStaging(param).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // alert(data.result)
          Swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          this.getUnscheduledWorkersList();
        },
        (error) => {
          console.log("error came", error)
          Swal.fire({
            title: 'Failed',
            text: 'Failed to save',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {
    }
  }

  public breaksummary(id) {
    // this.router.navigate(['/break-summary/' + id]);
    // this.bsmodelRef=this.modalService.show(BreakSummaryComponent,)
    this.bsmodelRef = this.modalService.show(BreaksummaryMobileComponent, {
      class: 'modal-container modal-dialog-centered unschedule-worker-modal-container',
      initialState: {
        id: id,
        data: {}
      }
    })

  }
  public navigate() {
    let arr=this.unscheduledWorkerList.filter(x=>x.stagingId==null);
    console.log(arr);
    if(arr.length>0){
      Swal.fire({
        title: 'Are you sure you want to Discard changes?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Discard',
      }).then((result) => {
        if (result.isConfirmed) {
    this.router.navigateByUrl('/roaster/' + this.eventID)

    }
  })
    }
    else{
    this.router.navigateByUrl('/roaster/' + this.eventID)

    }
  }
  public formError=false;
  public saveUnscheduledWorkerToStaging() {

    let validateFlag = true;
    let arr = [];
    arr = this.unscheduledWorkerList.map((x) => {
      console.log(x);
      let obj = {
        "ptId": x.personnelTypeId,
        "resourceId": x.resourceId,
        "agencyWorkerName": x.agencyWorker.agencyWorker ? x.agencyWorker.agencyWorker : x.agencyWorker,
        "agencyWorkerId": x.agencyWorkerId ? x.agencyWorkerId : 0,
        "stagingId": x.stagingId == null ? 0 : x.stagingId
      }
      if (x.personnelTypeId == null || x.resourceId == null) {
        validateFlag = false;
        this.formError=true;
      }
      return obj

    })
    let eventDetails = {
      "eventId": this.eventID,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId
    };
    let params = {
      "catererId": this.userdetails.catererId, "eventDetails": eventDetails, "unscheduledWorkersList": arr
    }
    if (validateFlag) {
    try {
      this.caterService.saveUnscheduledWorkerToStaging(params).subscribe(
        res => {
          console.log(res)
          this.getUnscheduledWorkersList();
          let data: any = res
          // alert(data.result);
          Swal.fire({
            title: 'success',
            text: data.result,
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });

        }
      )
    } catch (error) {

    }
    }else{
      Swal.fire('invalid','Please fill all highlighted fields before save','warning');

    }

  }
  public addUnschedule() {

    let object = {
      "agencyWorker": "",
      "personnelType": "",
      "personnelTypeCode": "",
      "personnelTypeId": null,
      "personnelTypeSortOrder": 0,
      "resourceId": null,
      "resourceName": "",
      "stagingId": null,
      "actCallIn": '',
      "actCallOut": ''

    }
    this.unscheduledWorkerList.push(object)
  }
  public deleteUnscheduledWorker(i) {

    let flag: boolean = this.unscheduledWorkerList[i].stagingId == null ? false : true;

    Swal.fire({
      title: 'Are you sure?',
      text: 'You Want to delete',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        if (flag) {
          try {
            this.caterService.deleteUnscheduledWorker(this.userdetails.catererId, this.unscheduledWorkerList[i].stagingId, this.userdetails.userId, this.userdetails.sessionId).subscribe(
              res => {
                console.log(res);
                Swal.fire({
                  title: 'success',
                  text: 'Deleted successfully',
                  icon: 'success',
                  timer: 1200,
                  showConfirmButton: false
                });
                this.getUnscheduledWorkersList();
              }
            )
          } catch (error) {

          }
        } else {
          this.unscheduledWorkerList.splice(i, 1)
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        console.log("not deleted")
      }
    })

  }
  public config = {
    backdrop: false,
    ignoreBackdropClick: false
  };
  public openFliter(template: TemplateRef<any>) {
    this.bsmodelRef = this.modalService.show(
      template,
      Object.assign(this.config, { class: 'un-popup' })
    );
  }
  public applyFilter() {
    this.formError=true;
      let arr = this.unscheduledWorkerList.filter(x => x.stagingId == null);
      console.log(arr);
      if (arr.length > 0) {
        Swal.fire({
          title: 'Are you sure you want to Discard changes?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Discard',
        }).then((result) => {
          if (result.isConfirmed) {
            // this.bsmodelRef.hide();
            this.getUnscheduledWorkersList();
          }
        })
      }
      else {
        // this.bsmodelRef.hide();
        this.getUnscheduledWorkersList();
      }


    this.bsmodelRef.hide();
  }

  public approveUnscheduledWorker(item) {
    try {
      let obj =
      {
        "stagingId": item.stagingId,
        "catererId": this.userdetails.catererId,
        "userId": this.userdetails.userId
      }
      this.caterService.approveUnscheduledWorker(obj).subscribe(res=>{
        console.log(res);
        if(res.sucess==1){
          Swal.fire('Success','Worker approved successfully','success');
          this.getUnscheduledWorkersList();

        }
      })


    } catch (error) {

    }
  }

}
