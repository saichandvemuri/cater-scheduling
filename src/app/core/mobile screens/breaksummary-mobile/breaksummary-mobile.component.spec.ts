import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreaksummaryMobileComponent } from './breaksummary-mobile.component';

describe('BreaksummaryMobileComponent', () => {
  let component: BreaksummaryMobileComponent;
  let fixture: ComponentFixture<BreaksummaryMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreaksummaryMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreaksummaryMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
