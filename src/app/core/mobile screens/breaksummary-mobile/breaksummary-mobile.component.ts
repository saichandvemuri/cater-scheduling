import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { CaterService } from 'src/app/services/cater.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-breaksummary-mobile',
  templateUrl: './breaksummary-mobile.component.html',
  styleUrls: ['./breaksummary-mobile.component.scss']
})
export class BreaksummaryMobileComponent implements OnInit {
  @ViewChild('template', { static: false }) childModal: ModalDirective;

  public userDetails: any;
  public stagingId;
  public workerBreaksList: Array<any> = [];
  public id;//coming from component roaster component as intial value
  public roasterdata;//coming from component roaster component as intial value
  public eventID;
  public bsmodelRef2: BsModalRef;
  public minutesArray: Array<number> = new Array<number>(60);

  popUpDate;
  popUpHours;
  popUpminutes;
  popUpMeridian;
  popupTypeTitle;
  popupType;
  popupTitle
  pastEventFlag:boolean=false
  deviceId: any;
  popupIndex;
  constructor(private caterService: CaterService, private activatedRoute: ActivatedRoute, public modalService: BsModalService, public datepipe: DatePipe) {
    this.userDetails = JSON.parse(sessionStorage.getItem('userdetails'));
    this.deviceId = JSON.parse(sessionStorage.getItem('ipAdd'));

  }

  ngOnInit(): void {
    this.getWorkerBreakList();
  }
  public getWorkerBreakList() {
    console.log(this.roasterdata)
    console.log(this.id)
    this.caterService.getWorkerBreakList(this.userDetails.catererId, this.id).subscribe(
      res => {
        console.log(res);
        let data: any = res;
        this.workerBreaksList = data.workerBreaksList;
      }
    )

  }

  public saveToSignInStaging(i,flag, popupFlag) {
    // console.log(this.workerBreaksList[i].break1ActOut,this.workerBreaksList[i].break1ActOut.length)
    let paramdatepopup = this.datepipe.transform(this.popUpDate, 'MM/dd/yyyy') + " " + this.popUpHours + ":" + this.popUpminutes + " " + this.popUpMeridian;
    let param = {
      "eventId": this.eventID,
      "resourceID": this.roasterdata.resourceId,
      "scheduleID": this.roasterdata.scheduleId,
      "scheduledFlag": 1,
      "actualCallIn": this.roasterdata.actCallIn,
      "actualCallOut": this.roasterdata.actCallOut,
      "break1ActIns": flag == "break1in" ? paramdatepopup : this.workerBreaksList[i].break1ActIn !=undefined?this.workerBreaksList[i].break1ActIn:'',
      "break1ActOuts": flag == "break1out" ? paramdatepopup : this.workerBreaksList[i].break1ActOut !=undefined?this.workerBreaksList[i].break1ActOut:'',
      "break2ActIns": flag == "break2in" ? paramdatepopup : this.workerBreaksList[i].break2ActIn !=undefined?this.workerBreaksList[i].break1ActIn:'',
      "break2ActOuts": flag == "break2out" ? paramdatepopup : this.workerBreaksList[i].break2ActOut !=undefined?this.workerBreaksList[i].break2ActOut:'',
      "break3ActIns": flag == "break3in" ? paramdatepopup : this.workerBreaksList[i].break3ActIn !=undefined?this.workerBreaksList[i].break3ActIn:'',
      "break3ActOuts": flag == "break3out" ? paramdatepopup : this.workerBreaksList[i].break3ActOut !=undefined?this.workerBreaksList[i].break3ActOut:'',
      "personnelTypeId": this.roasterdata.personnelTypeId,
      "agencyWorkerId": this.roasterdata.agencyWorkerId,
      "agencyWorker": this.roasterdata.agencyWorker,
      "biometricflag": this.roasterdata.biometricFlag,
      "userId": this.userDetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userDetails.sessionId,
      "catererId": this.userDetails.catererId,
      "stagingId": this.roasterdata.stagingId

    }
    try {
      this.caterService.saveToSignInStaging(param).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // alert(data.result)
        this.bsmodelRef2.hide()
        this.bsmodelRef2.hide()

          // this.childModal.hide();
          swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          this.getWorkerBreakList()
        },
        (error) => {
          console.log("error came", error)
          swal.fire({
            title: 'Failed',
            text: 'Failed to save',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {
    }
  }

  public signInFlag(template: TemplateRef<any>, i, flag, date) {
  //   this.popupIndex = i;

  //   this.popupType=flag;
  //   if (flag == 'break1in') {
  //     this.popupTypeTitle = 'Break1 In';
  //   } else if (flag == 'break2in') {
  //     this.popupTypeTitle = 'Break2 In';
  //   } else if (flag == 'break3in') {
  //     this.popupTypeTitle = 'Break3 In';
  //   } else if (flag == 'break1out') {
  //     this.popupTypeTitle = 'Break1 Out';
  //   } else if (flag == 'break2out') {
  //     this.popupTypeTitle = 'Break2 Out';
  //   } else if (flag == "break3out") {
  //     this.popupTypeTitle = 'Break3 Out';
  //   } else if (flag == 'signin') {
  //     this.popupTypeTitle = 'Sign In'
  //   } else if (flag == 'signout') {
  //     this.popupTypeTitle = 'Sign Out'
  //   }
  //   console.log(this.popupType)
  //   this.popupTitle = this.workerBreaksList[i].resourceName;
  //  console.log(date)
  //   let condition:string = date;
  //   console.log(condition)
  //   console.log(condition.length)
  //   if (!this.pastEventFlag) {
  //     if (condition == undefined || condition.length == 0) {
  //       let currentdate = new Date()
  //       // this.saveToSignInStaging(i, flag, false)
  //       this.popUpminutes = +this.datepipe.transform(currentdate, 'mm');
  //       this.popUpHours = +this.datepipe.transform(currentdate, 'hh');
  //       this.popUpMeridian = this.datepipe.transform(currentdate, 'a');
  //       this.popUpDate = this.datepipe.transform(currentdate, 'MM/dd/yyyy')
  //       this.bsmodelRef2 = this.modalService.show(template, Object.assign({
  //         backdrop: false,
  //         animated: true,
  //         keyboard: false,
  //         ignoreBackdropClick: true,
  //       }, { class: 'modal-container modal-dialog-centered' }));

  //     }
  //     else {
  //       if (condition.trim().length > 0) {
  //         // this.popUpminutes = +this.datepipe.transform(date, 'mm');
  //         // this.popUpHours = +this.datepipe.transform(date, 'hh');
  //         // this.popUpMeridian = this.datepipe.transform(date, 'a');
  //         this.popUpDate = this.datepipe.transform(this.roasterdata.actCallIn, 'MM/dd/yyyy')
  //         this.popUpHours=condition.split(':')[0]
  //         this.popUpminutes=condition.split(':')[1].split(' ')[0]
  //         this.popUpMeridian=condition.split(':')[1].split(' ')[1]
  //         // this.childModal.show();
  //         this.bsmodelRef2 = this.modalService.show(template, Object.assign({
  //           backdrop: false,
  //           // animated: true,
  //           // keyboard: false,
  //           ignoreBackdropClick: true,
  //         }, { class: 'modal-container modal-dialog-centered' }));

  //       }
  //     }
  //   }
  }

}
