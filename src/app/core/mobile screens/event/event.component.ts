import { DatePipe } from '@angular/common';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CaterService } from 'src/app/services/cater.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  public windowHeight = (window.innerHeight - 130) + 'px';
  public modelref: BsModalRef;
  public userdetails: any;
  public eventList: Array<any> = [];
  public staffingInfo: Array<any> = [];
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public currentDate: Date = new Date();
  public cisNumber = null;
  public eventDetails: any;
  public blobcontentImg;
  public blobcontent: boolean = false;
  public  ipAddress;


  public eventFilterDetails;
  public queryParam;

  constructor(private route: ActivatedRoute, public caterService: CaterService, public modelService: BsModalService, public datepipe: DatePipe, public router: Router, private deviceService: DeviceDetectorService) {
    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'));
    this.eventFilterDetails = JSON.parse(sessionStorage.getItem('eventFilterDetails'));
    this.ipAddress=JSON.parse(sessionStorage.getItem('ipAdd'));

    console.log("rrrr")

    if (this.userdetails) {
      console.log("from session", this.userdetails)
    } else {
      this.route.queryParams.subscribe(params => {
        this.userdetails = {
          active: params['active'],
          allowAccess: params['allowAccess'],
          allowBreaks: params['allowBreaks'],
          catererId: params['catererId'],
          catererName: params['catererName'],
          sessionId: params['sessionId'],
          userId: params['userId'],
          isBrowser: params['isBrowser']
        }
        console.log(this.userdetails.sessionId)
        if (this.userdetails.sessionId) {
          console.log("session set")
          sessionStorage.setItem('userdetails', JSON.stringify(this.userdetails))
        }
      });

    }
    if (this.eventFilterDetails) {
      console.log(this.eventFilterDetails);

      this.startDate = new Date(this.eventFilterDetails.startDate);
      this.endDate = new Date(this.eventFilterDetails.endDate);
      this.cisNumber = this.eventFilterDetails.cisNumber;
      this.getEventList();

    } else {
      console.log("constructer")
      this.getFilterPreferences();
    }
  }

  ngOnInit(): void {
    if(this.ipAddress){
      // this.getEventList();
    }else{
    try {
      console.log("eventlist")
      this.caterService.getIpAddresss().subscribe(res => {
        let data: any = res;
       this.getEventList();
      },err=>{
      })

    } catch (error) {

    }
  }

  }
  public getFilterPreferences(){
    try {
      console.log("GET Filter prefernces")
      this.caterService.getFilterPreferences(this.userdetails.catererId,this.userdetails.userId).subscribe(res=>{
        console.log(res);
        this.startDate=new Date();
        this.endDate=new Date();
        this.startDate.setDate(new Date().getDate()-res.pastFilterDays);
        this.endDate.setDate(new Date().getDate()+res.futureFilterDays);
        this.getEventList();
      })
    } catch (error) {

    }

  }
  public getEventList() {
    try {
      console.log("eventlist")

      this.caterService.getEventList(this.userdetails.catererId, (this.cisNumber == null || this.cisNumber == '' ? "" : this.cisNumber), this.datepipe.transform(this.startDate, 'MM/dd/yyyy'), this.datepipe.transform(this.endDate, 'MM/dd/yyyy'), this.userdetails.userId, this.userdetails.sessionId).subscribe(
        resp => {
          console.log(resp);
          let response: any = resp;
          this.eventList = response.eventList;
          this.eventList.map(obj=>{
            if(obj.submitFlag==1){
              obj.bgColor='tr-bg-green';
            }else if((obj.signInCount==obj.noOfWorkers)&&(obj.noOfWorkers==obj.signOutCount)){
              obj.bgColor='tr-bg-red';
            }
          })

        }
      )

    } catch (error) {

    }
  }

  public getEventStaffingInfo(eventid: number, event) {
    this.eventDetails = event;
    sessionStorage.setItem("eventdetails", JSON.stringify(event))
    console.log(eventid);
    try {
      this.caterService.getEventStaffingInfo(this.userdetails.catererId, eventid).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.staffingInfo = data.staffingInfo;
          console.log(this.staffingInfo);
        }
      )
    } catch (error) {

    }
  }

  public roster(eventId, event) {
    sessionStorage.setItem("eventdetails", JSON.stringify(event))
    // this.router.navigate(['/roaster/' + eventId]);
    this.router.navigate(['/roaster/' + eventId])

  }

  public applyfilter() {
    console.log("filterworking")
    let obj = { 'endDate': this.datepipe.transform(this.endDate, 'MM/dd/yyyy'), 'startDate': this.datepipe.transform(this.startDate, 'MM/dd/yyyy'), 'cisNumber': (this.cisNumber == null || '' ? "" : this.cisNumber) }
    sessionStorage.setItem('eventFilterDetails', JSON.stringify(obj));
    if(Date.parse(this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))<Date.parse(this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))){
     Swal.fire('Invalid','Start date cannot be greater than end date','warning');
    }else{
    this.getEventList();
    }
  }
  public getEventQRCode(event, template: TemplateRef<any>) {

    if (this.userdetails.isBrowser) {
      console.log("qrc", window.location.origin)
      let url = window.location.origin;
      let pathurl = url + '/SupervisorSignIn/resources/SupervisorSignIn/SupervisorSignIn.html#/qrc/' + event.eventid+'/'+ (event.nonEventFlag==1?event.eventDate:'');
      window.open(pathurl)

    } else {
      try {
        this.caterService.getEventQRCode(this.userdetails.catererId, event.eventid,event.nonEventFlag==1?this.datepipe.transform(event.eventDate,'MM/dd/yyyy'):'').subscribe(res => {
          // console.log(res)
          let data: any = res;
          // console.log(data.eventQRCode)
          this.blobcontentImg = data.eventQRCode;
          this.blobcontent = true;
          this.modelref = this.modelService.show(template);

        })
      } catch (error) {

      }
    }
  }
}
