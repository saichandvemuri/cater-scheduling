import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CaterService } from 'src/app/services/cater.service';

@Component({
  selector: 'app-mobile-header',
  templateUrl: './mobile-header.component.html',
  styleUrls: ['./mobile-header.component.scss']
})
export class MobileHeaderComponent implements OnInit {

  public userdetails: any;
  constructor(private caterService: CaterService,private router:Router,public devicedetector:DeviceDetectorService) {
    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'))
    console.log(devicedetector.isDesktop(),devicedetector.isMobile())
  }

  ngOnInit(): void {
  }
  logOutUser() {
    try {
      this.caterService.logOutUser(this.userdetails.catererId, this.userdetails.sessionId).subscribe(
        res => {
          console.log(res)
          setTimeout(() => {
            this.router.navigateByUrl('login');
            sessionStorage.removeItem('userdetails');
            sessionStorage.removeItem('eventFilterDetails');
            sessionStorage.removeItem('eventdetails');
            sessionStorage.removeItem('ipAdd')

          }, 100);
        }
      )
    } catch (error) {

    }
  }
public home(){
  if(this.devicedetector.isMobile()){
    this.router.navigateByUrl('event');

  }else{
    console.log(this.devicedetector.isDesktop());
    this.router.navigateByUrl('events');

  }
}

}
