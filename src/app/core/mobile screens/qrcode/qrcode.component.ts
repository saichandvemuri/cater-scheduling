import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CaterService } from 'src/app/services/cater.service';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.scss']
})
export class QrcodeComponent implements OnInit {
  public userdetails: any;
  public blobcontentImg: any;
  public blobcontent: boolean = false;
  public eventID: number;
  public eventDate='';
  constructor(private activatedRoute: ActivatedRoute, public caterService: CaterService, public modelService: BsModalService, public datepipe: DatePipe, public router: Router, private deviceService: DeviceDetectorService) {
    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'));
    this.eventID = this.activatedRoute.snapshot.params.eventId;
    this.eventDate=this.activatedRoute.snapshot.params.eventDate;
    this.eventDate=this.datepipe.transform(this.eventDate,'MM/dd/yyy')
    console.log(this.eventDate)


  }
  ngOnInit(): void {
    this.getEventQRCode(this.eventID)
  }
  public getEventQRCode(eventID, template?: TemplateRef<any>) {
    console.log("qrc", window.location.origin)
    try {
      this.caterService.getEventQRCode(this.userdetails.catererId, eventID,this.eventDate==null?'':this.eventDate).subscribe(res => {
        // console.log(res)
        let data: any = res;
        // console.log(data.eventQRCode)
        this.blobcontentImg = data.eventQRCode;
        this.blobcontent = true;
        // this.modelref = this.modelService.show(template);

      })
    } catch (error) {

    }
  }
  public close() {
    self.close();
    window.close();

  }


}
