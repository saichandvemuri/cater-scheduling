import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakSummaryComponent } from './break-summary.component';

describe('BreakSummaryComponent', () => {
  let component: BreakSummaryComponent;
  let fixture: ComponentFixture<BreakSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreakSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
