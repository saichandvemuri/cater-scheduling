import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CaterService } from 'src/app/services/cater.service';

@Component({
  selector: 'app-break-summary',
  templateUrl: './break-summary.component.html',
  styleUrls: ['./break-summary.component.scss']
})
export class BreakSummaryComponent implements OnInit {

  public userDetails:any;
  public stagingId;
  public workerBreaksList:Array<any>=[];
  public id;
  public bsmodelRef: BsModalRef;


  constructor(private caterService:CaterService,private activatedRoute: ActivatedRoute, public modalService: BsModalService) {
    this.userDetails=JSON.parse(sessionStorage.getItem('userdetails'));
   }

  ngOnInit(): void {
    this.getWorkerBreakList();
  }
  public getWorkerBreakList(){
    this.caterService.getWorkerBreakList(this.userDetails.catererId,this.id).subscribe(
      res=>{
        console.log(res);
        let data:any=res;
        this.workerBreaksList=data.workerBreaksList;
      }
    )

  }
}
