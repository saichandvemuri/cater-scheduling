import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CaterService } from 'src/app/services/cater.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  public userdetails: any;
  public eventList: Array<any> = [];
  public staffingInfo: Array<any> = [];
  public startDate: Date;
  public endDate: Date;
  public currentDate: Date = new Date();
  public cisNumber: number = null;
  public eventDetails: any;

  public eventFilterDetails;

  constructor(public route: ActivatedRoute, public caterService: CaterService, public datepipe: DatePipe, public router: Router, private deviceService: DeviceDetectorService) {
    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'));
    this.eventFilterDetails = JSON.parse(sessionStorage.getItem('eventFilterDetails'));
    if (this.userdetails) {
      console.log("from session", this.userdetails)
    } else {
      this.route.queryParams.subscribe(params => {
        this.userdetails = {
          active: params['active'],
          allowAccess: params['allowAccess'],
          allowBreaks: params['allowBreaks'],
          catererId: params['catererId'],
          catererName: params['catererName'],
          sessionId: params['sessionId'],
          userId: params['userId']
        }
        sessionStorage.setItem('userdetails', JSON.stringify(this.userdetails))
      });
    }
    if (this.eventFilterDetails) {
      console.log(this.eventFilterDetails);

      this.startDate = new Date(this.eventFilterDetails.startDate);
      this.endDate = new Date(this.eventFilterDetails.endDate);
      this.cisNumber = this.eventFilterDetails.cisNumber;
      this.getEventList()

    } else {
      console.log(this.eventFilterDetails)
      this.getFilterPreferences();
    }

  }
  ngOnInit(): void {



  }

  public getEventList() {
      try {
      this.caterService.getEventList(this.userdetails.catererId, (this.cisNumber == null || '' ? "" : this.cisNumber), this.datepipe.transform(this.startDate, 'MM/dd/yyyy'), this.datepipe.transform(this.endDate, 'MM/dd/yyyy'), this.userdetails.userId, this.userdetails.sessionId).subscribe(
        resp => {
          console.log(resp);
          let response: any = resp;
          this.eventList = response.eventList;
          this.eventList.map(obj=>{
            if(obj.submitFlag==1){
              obj.bgColor='tr-bg-green';
            }else if((obj.signInCount==obj.noOfWorkers)&&(obj.noOfWorkers==obj.signOutCount)){
              obj.bgColor='tr-bg-red';
            }
          })
          console.log(this.eventList)

        }
      )
    } catch (error) {

    }
  }
  public getFilterPreferences(){
    try {
      this.caterService.getFilterPreferences(this.userdetails.catererId,this.userdetails.userId).subscribe(res=>{
        this.startDate=new Date();
        this.endDate=new Date();
       this.startDate.setDate(new Date().getDate()-(res.pastFilterDays));
        this.endDate.setDate(new Date().getDate()+(res.futureFilterDays));
        console.log(this.startDate,this.endDate)
        this.getEventList();
      })
    } catch (error) {

    }

  }

  public getEventStaffingInfo(eventid: number, event) {
    this.eventDetails = event;
    console.log(eventid);
    try {
      this.caterService.getEventStaffingInfo(this.userdetails.catererId, eventid).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.staffingInfo = data.staffingInfo;
          console.log(this.staffingInfo);
        }
      )
    } catch (error) {

    }
  }

  public roster(eventId, event) {
    sessionStorage.setItem("eventdetails", JSON.stringify(event))
    this.router.navigate(['/roster/' + eventId]);

  }

  public applyfilter() {
    let obj = { 'endDate': this.datepipe.transform(this.endDate, 'MM/dd/yyyy'), 'startDate': this.datepipe.transform(this.startDate, 'MM/dd/yyyy'), 'cisNumber': (this.cisNumber == null || '' ? "" : this.cisNumber) }
    sessionStorage.setItem('eventFilterDetails', JSON.stringify(obj));
    if(Date.parse(this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))<Date.parse(this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))){
    Swal.fire('Invalid','Start date cannot be greater than end date','warning');
     }else{
     this.getEventList();
     }
  }
}
