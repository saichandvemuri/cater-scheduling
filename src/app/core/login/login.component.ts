import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CaterService } from '../../services/cater.service';
import swal from 'sweetalert2';
import { DeviceDetectorService } from 'ngx-device-detector';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private _fb: FormBuilder, public apiservice: CaterService, private router: Router, private deviceService: DeviceDetectorService) {
    let data = sessionStorage.getItem('userdetails');
    if (data) {
      this.router.navigateByUrl('events')
    }
  }
  public loginForm: FormGroup;
  public formError: boolean = false;
  public responsedata: any;
  public ShowPassword: boolean = false;



  ngOnInit(): void {
    let deviceInfo = this.deviceService.getDeviceInfo();
    console.log(deviceInfo)

    // this.channelChanged();
    this.loginForm = this._fb.group({
      catererId: new FormControl('', [Validators.required]),
      userId: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })


  }

  get form() {
    return this.loginForm.controls;
  }
  public login() {
    this.loginForm.get('catererId').setValue(this.loginForm.value.catererId.trim())
    this.loginForm.get('userId').setValue(this.loginForm.value.userId.trim())

    console.log(this.loginForm.value.catererId)
    this.formError = true;
    if (this.loginForm.valid) {
      let userid: string = this.loginForm.value.userId, caterid: string = this.loginForm.value.catererId;
      userid = userid.replace(/ /g, '');
      caterid = caterid.replace(/ /g, '').toLocaleLowerCase();
      try {
        this.apiservice.authenticateUser(userid, this.loginForm.value.password, caterid).subscribe(
          response => {
            let data: any = response;
            console.log(response);
            this.responsedata = response;
            console.log(data.userId != '0');
            if (data.AuthenticateUser) {
              if (data.AuthenticateUser.userId != 0) {
                this.responsedata.AuthenticateUser.catererId = caterid;
                this.responsedata.AuthenticateUser.isBrowser = true;
                if (this.deviceService.isMobile()) {
                  this.router.navigate(['/event'], {
                    queryParams: {
                      active: this.responsedata.AuthenticateUser.active,
                      allowAccess: this.responsedata.AuthenticateUser.allowAccess,
                      allowBreaks: this.responsedata.AuthenticateUser.allowBreaks,
                      catererId: caterid,
                      catererName: this.responsedata.AuthenticateUser.catererName,
                      sessionId: this.responsedata.AuthenticateUser.sessionId,
                      userId: this.responsedata.AuthenticateUser.userId,
                      isBrowser: true
                    }
                  });
                } else {
                  sessionStorage.setItem('userdetails', JSON.stringify(this.responsedata.AuthenticateUser))
                  this.router.navigate(['/events'], {
                    queryParams: {
                      active: this.responsedata.AuthenticateUser.active,
                      allowAccess: this.responsedata.AuthenticateUser.allowAccess,
                      allowBreaks: this.responsedata.AuthenticateUser.allowBreaks,
                      catererName: this.responsedata.AuthenticateUser.catererName,
                      sessionId: this.responsedata.AuthenticateUser.sessionId,
                      userId: this.responsedata.AuthenticateUser.userId,
                      catererId: caterid
                    }
                  });

                }
              }
              else {
                swal.fire({
                  title: 'Invalid User  ',
                  text: 'Login Failed',
                  icon: 'error',
                  // timer: 1200,
                  showConfirmButton: true
                });
              }
            } else {
              swal.fire({
                title: 'Invalid Caterer Id',
                text: 'Login Failed',
                icon: 'error',
                // timer: 1200,
                showConfirmButton: true
              });
            }

          },
          error => {
            swal.fire({
              title: 'Invalid User',
              text: 'Login Failed',
              icon: 'error',
              // timer: 1200,
              showConfirmButton: true
            });
          }
        )

      }
      catch (error) {
        console.log(error);
      }




    }
  }
  public showpassword() {
    this.ShowPassword = !this.ShowPassword;
  }
  channelChanged() {
    var card = document.querySelector('.aa');
    // card.addEventListener( 'click', function() {
    card.classList.toggle('is-flipped');
    // });
  }

}
