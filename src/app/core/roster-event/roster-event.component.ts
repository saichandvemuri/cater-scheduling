import { DatePipe } from '@angular/common';
import { Component, DoCheck, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CaterService } from 'src/app/services/cater.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import swal from 'sweetalert2';
import { BreakSummaryComponent } from '../break-summary/break-summary.component';
import { Observable } from 'rxjs/internal/Observable';
import { interval, Observer, of, Subscriber, Subscription } from 'rxjs';
import { TypeaheadMatch, TypeaheadOptionItemContext, TypeaheadOptionListContext } from 'ngx-bootstrap/typeahead';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, mergeMap, map } from 'rxjs/operators';
import { HoursDifference } from 'src/app/pipes/domsanitize.pipe';
declare var $: any

@Component({
  selector: 'app-roster-event',
  templateUrl: './roster-event.component.html',
  styleUrls: ['./roster-event.component.scss']
})
export class RosterEventComponent implements OnInit {
  @ViewChild('template') modal;
  subscription: Subscription;
  roaster = "roaster";
  public pastEventFlag: boolean = false;

  public positionTypeId: number = null;
  public positionTypeIdunScheduled: number = null;
  public personnelTypesListRoster: Array<any> = [];
  public bsmodelRef: BsModalRef;
  public userdetails: any;
  public mode: number = 1;
  public workerName = null;
  public workerNameUnscheduled = null;
  public eventID;
  public minutesArray: Array<number> = new Array<number>(60);
  public popupTitle: string = '';
  public placeholder = 'Position Type'

  public breakCount: number;
  public noOfWorkers: number;
  public signInCount: number;
  public signOutCount: number;
  public rosterData: Array<any> = [];
  public deviceId: any;

  public unscheduledWorkerList: Array<any> = [];

  public personnelTypesList: Array<any> = [];
  public workerList: Array<any> = [];
  public blobcontent: boolean = false;
  public blobcontentImg;

  public positionsort: boolean = true;
  public workersort: boolean = true;
  public positionsortunschedule: boolean = true;
  public workersortunschedule: boolean = true;



  public popUpminutes = null;
  public popUpHours = null;
  public popUpMeridian = null;
  public popUpDate;
  public popupIndex: number;
  public popupType: string;
  public unScheduleFlag: boolean = false;

  public currentTime: Date;
  public eventDetails: any;

  public agencyWorkerList: DataSourceType[] = [];
  agencyWokerListIndex
  public agencyWorkerLabel: string = "agencyWorker";
  public eventDate = null;
  public unscheduledEventDate = '';
  public intialAPiCall: boolean = true;
  public totalScheduleHours = null;
  public totalActualHours = null;


  constructor(private router: Router, private modalService: BsModalService, private toastr: ToastrService,
    public caterService: CaterService, public datepipe: DatePipe, private activatedRoute: ActivatedRoute,
    public hoursDifference: HoursDifference) {

    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'));
    this.deviceId = JSON.parse(sessionStorage.getItem('ipAdd'));
    this.eventDetails = JSON.parse(sessionStorage.getItem('eventdetails'));

    this.unscheduledEventDate = this.eventDate;

    let eventDate = Date.parse(this.eventDetails.eventDate);
    let systemdate = Date.parse(this.datepipe.transform(new Date(), 'MM/dd/yyyy'))
    console.log(eventDate, systemdate, eventDate > systemdate, this.eventDetails.enableSubmitFlag >= 9)
    if ((this.eventDetails.enableSubmitFlag == 1)) {
      //condtion will allow
      this.pastEventFlag = false;
    } else {
      // condition will not allow to call apis
      this.pastEventFlag = true;
    }

    this.eventID = this.activatedRoute.snapshot.params.eventId;
    console.log(this.eventID)



  }



  ngOnInit(): void {
    if (this.eventDetails.nonEventFlag == 1) {

    } else {
      const source = interval(300000);
      this.subscription = source.subscribe(val => this.getRosterData());
      this.getRosterData();
    }


  }
  public intialstart = 0;
  public onGo() {
    console.log(this.eventDate)
    if (this.eventDate == null) {
      swal.fire('', 'Please select Event date and click on search', 'warning')
    } else {
      if (this.intialstart == 0) {
        console.log("subscription")
        const source = interval(300000);
        this.subscription = source.subscribe(val => this.getRosterData());
      }
      this.intialstart++;
      this.getRosterData()
    }
  }
  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

  //catererId=ridgewellscat&eventId=825241&sessionId=528127622&workerName=a&mode=1
  public getRosterData() {
    this.intialAPiCall = false;
    let date = this.datepipe.transform(this.eventDate, 'MM/dd/yyyy');
    try {
      this.caterService.getRosterData(this.userdetails.catererId, this.eventID, this.userdetails.sessionId, this.workerName == null ? '' : this.workerName, this.mode, this.positionTypeId == null ? '' : this.positionTypeId, this.eventDetails.nonEventFlag == 1 && date != null ? this.datepipe.transform(this.eventDate, 'MM/dd/yyyy') : '').subscribe(
        res => {
          console.log(res)
          if (res) {
            let data: any = res;
            this.breakCount = data.countsData.breakCount;
            this.noOfWorkers = data.countsData.noOfWorkers;
            this.signInCount = data.countsData.signInCount;
            this.signOutCount = data.countsData.signOutCount;
            this.rosterData = data.rosterData;
            this.unscheduledWorkerList = data.unscheduledWorkerList
            this.personnelTypesListRoster = data.personnelTypesList;
            console.log(this.rosterData)
            this.blobcontent = true;
            this.blobcontentImg = data.eventQRCode.blobcontent;
            let totalScheduleMins = 0;
            let totalActualMins = 0;

            for (let i = 0; i < this.rosterData.length; i++) {
              if (this.rosterData[i].break1Flag == 0) {
                this.rosterData[i].breakvalue = "start"
                this.rosterData[i].breakTaken = 0;
              } else if (this.rosterData[i].break1Flag == 1) {
                this.rosterData[i].breakvalue = "end"
                this.rosterData[i].breakTaken = 1;
              } else if (this.rosterData[i].break1Flag == 2) {
                if (this.rosterData[i].break2Flag == 0) {
                  this.rosterData[i].breakvalue = "start"
                  this.rosterData[i].breakTaken = 1;
                } else if (this.rosterData[i].break2Flag == 1) {
                  this.rosterData[i].breakvalue = "end"
                  this.rosterData[i].breakTaken = 2;
                }
                else if (this.rosterData[i].break2Flag == 2) {
                  if (this.rosterData[i].break3Flag == 0) {
                    this.rosterData[i].breakvalue = "start"
                    this.rosterData[i].breakTaken = 2;
                  } else if (this.rosterData[i].break3Flag == 1) {
                    this.rosterData[i].breakvalue = "end"
                    this.rosterData[i].breakTaken = 3;
                  }
                  else if (this.rosterData[i].break3Flag == 2) {
                    this.rosterData[i].breakTaken = 3;
                    this.rosterData[i].breakvalue = "complete"
                  }
                }
              }
              totalScheduleMins += this.hoursDifference.transform(this.datepipe.transform(this.rosterData[i].callIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(this.rosterData[i].callOut, 'MM/dd/yyyy h:mm a'));
              if (this.rosterData[i].actCallOut.length > 0 && this.rosterData[i].actCallIn.length > 0) {
                this.rosterData[i].totalActualMinutes = this.hoursDifference.transform(this.datepipe.transform(this.rosterData[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(this.rosterData[i].actCallOut, ' MM/dd/yyyy h:mm a'));
                totalActualMins += this.hoursDifference.transform(this.datepipe.transform(this.rosterData[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(this.rosterData[i].actCallOut, ' MM/dd/yyyy h:mm a'));
              }else if(this.rosterData[i].actCallOut.length == 0 && this.rosterData[i].actCallIn.length > 0){
                // this.rosterData[i].totalActualMinutes = this.hoursDifference.transform(this.datepipe.transform(this.rosterData[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(new Date(), ' MM/dd/yyyy h:mm a'));
                totalActualMins += this.hoursDifference.transform(this.datepipe.transform(this.rosterData[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(new Date(), ' MM/dd/yyyy h:mm a'));
              }

            }


            for (let i = 0; i < this.unscheduledWorkerList.length; i++) {
              if (this.unscheduledWorkerList[i].break1Flag == 0) {
                this.unscheduledWorkerList[i].breakvalue = "start"
                this.unscheduledWorkerList[i].breakTaken = 0;
              } else if (this.unscheduledWorkerList[i].break1Flag == 1) {
                this.unscheduledWorkerList[i].breakvalue = "end"
                this.unscheduledWorkerList[i].breakTaken = 1;
              } else if (this.unscheduledWorkerList[i].break1Flag == 2) {
                if (this.unscheduledWorkerList[i].break2Flag == 0) {
                  this.unscheduledWorkerList[i].breakvalue = "start"
                  this.unscheduledWorkerList[i].breakTaken = 1;
                } else if (this.unscheduledWorkerList[i].break2Flag == 1) {
                  this.unscheduledWorkerList[i].breakvalue = "end"
                  this.unscheduledWorkerList[i].breakTaken = 2;
                }
                else if (this.unscheduledWorkerList[i].break2Flag == 2) {
                  if (this.unscheduledWorkerList[i].break3Flag == 0) {
                    this.unscheduledWorkerList[i].breakvalue = "start"
                    this.unscheduledWorkerList[i].breakTaken = 2;
                  } else if (this.unscheduledWorkerList[i].break3Flag == 1) {
                    this.unscheduledWorkerList[i].breakvalue = "end"
                    this.unscheduledWorkerList[i].breakTaken = 3;
                  }
                  else if (this.unscheduledWorkerList[i].break3Flag == 2) {
                    this.unscheduledWorkerList[i].breakTaken = 3;
                    this.unscheduledWorkerList[i].breakvalue = "complete"
                  }
                }
              }
              if (this.unscheduledWorkerList[i].actCallOut.length > 0 && this.unscheduledWorkerList[i].actCallIn.length > 0) {
                // this.unscheduledWorkerList[i].totalActualMinutes = this.hoursDifference.transform(this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy h:mm a'));
                totalActualMins += this.hoursDifference.transform(this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy h:mm a'));
              }else if(this.unscheduledWorkerList[i].actCallOut.length == 0 && this.unscheduledWorkerList[i].actCallIn.length > 0){
                // this.unscheduledWorkerList[i].totalActualMinutes = this.hoursDifference.transform(this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(new Date(), ' MM/dd/yyyy h:mm a'));
                totalActualMins += this.hoursDifference.transform(this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy h:mm a'), this.datepipe.transform(new Date(), ' MM/dd/yyyy h:mm a'));
              }

            }
            this.totalScheduleHours = (totalScheduleMins/60).toFixed(2);
            this.totalActualHours =(totalActualMins/60).toFixed(2);

          }


        }
      )
    } catch (error) {

    }

  }
  public signInFlag(template: TemplateRef<any>, i, flag) {
    this.currentTime = new Date();
    this.popupIndex = i;
    this.popupType = flag;
    this.popupTitle = this.rosterData[i].resourceName;

    let condition = flag == "signin" ? this.rosterData[i].actCallIn : this.rosterData[i].actCallOut;
    console.log(condition)
    if (!this.pastEventFlag) {

      if (condition == undefined) {
        this.saveToSignInStaging(i, flag, false)
      }
      else {
        if (condition.trim().length > 0) {
          this.popUpminutes = +this.datepipe.transform(flag == "signin" ? this.rosterData[i].actCallIn : this.rosterData[i].actCallOut, 'mm');
          this.popUpHours = +this.datepipe.transform(flag == "signin" ? this.rosterData[i].actCallIn : this.rosterData[i].actCallOut, 'hh');
          this.popUpMeridian = this.datepipe.transform(flag == "signin" ? this.rosterData[i].actCallIn : this.rosterData[i].actCallOut, 'a');
          this.popUpDate = this.datepipe.transform(flag == "signin" ? this.rosterData[i].actCallIn : this.rosterData[i].actCallOut, 'MM/dd/yyyy')
          this.bsmodelRef = this.modalService.show(template, Object.assign({}, { class: 'modal-container modal-dialog-centered' }));

        } else {
          this.saveToSignInStaging(i, flag, false)
        }
      }
    }
  }

  public getUnscheduledWorkersList() {
    this.formError = false;
    let date = this.datepipe.transform(this.eventDate, 'MM/dd/yyyy');
    try {
      this.caterService.getUnscheduledWorkersList(this.userdetails.catererId, this.workerNameUnscheduled == null ? '' : this.workerNameUnscheduled, this.userdetails.userId, this.userdetails.sessionId, this.eventID, this.positionTypeIdunScheduled == null ? '' : this.positionTypeIdunScheduled, this.eventDetails.nonEventFlag == 1 && date != null ? this.datepipe.transform(this.eventDate, 'MM/dd/yyyy') : '').subscribe(
        res => {
          console.log(res)
          let data: any = res;
          this.workerList = data.workerList;
          this.unscheduledWorkerList = data.unscheduledWorkerList;
          this.personnelTypesList = data.personnelTypesList;
          console.log(this.personnelTypesList)
          let i = 0;
          this.unscheduledWorkerList.map((x) => {
            if (x.break1Flag == 0) {
              this.unscheduledWorkerList[i].breakvalue = "start"
              this.unscheduledWorkerList[i].breakTaken = 0;
            } else if (x.break1Flag == 1) {
              this.unscheduledWorkerList[i].breakvalue = "end"
              this.unscheduledWorkerList[i].breakTaken = 1;
            } else if (x.break1Flag == 2) {
              if (x.break2Flag == 0) {
                this.unscheduledWorkerList[i].breakvalue = "start"
                this.unscheduledWorkerList[i].breakTaken = 1;
              } else if (x.break2Flag == 1) {
                this.unscheduledWorkerList[i].breakvalue = "end"
                this.unscheduledWorkerList[i].breakTaken = 2;
              }
              else if (x.break2Flag == 2) {
                if (x.break3Flag == 0) {
                  this.unscheduledWorkerList[i].breakvalue = "start"
                  this.unscheduledWorkerList[i].breakTaken = 2;
                } else if (x.break3Flag == 1) {
                  this.unscheduledWorkerList[i].breakvalue = "end"
                  this.unscheduledWorkerList[i].breakTaken = 3;
                }
                else if (x.break3Flag == 2) {
                  this.unscheduledWorkerList[i].breakTaken = 3;
                  this.unscheduledWorkerList[i].breakvalue = "complete"
                }
              }
            }
            i++;
          })

        }
      )
    } catch (error) {

    }
  }

  public onUnscheduleSearch(){
    let arr = this.unscheduledWorkerList.filter(x => x.stagingId == null);
    console.log(arr);
    if (arr.length > 0) {
      swal.fire({
        title: 'Are you sure you want to Discard changes?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Discard',
      }).then((result) => {
        if (result.isConfirmed) {
          this.getUnscheduledWorkersList();

        }
      })
    }
    else {
      this.getUnscheduledWorkersList();

    }

  }

  public addUnschedule() {

    let object = {
      "agencyWorker": "",
      "personnelType": "",
      "personnelTypeCode": "",
      "personnelTypeId": null,
      "personnelTypeSortOrder": 0,
      "resourceId": null,
      "resourceName": "",
      "stagingId": null,
      "actCallIn": '',
      "actCallOut": '',
      "agencyWorkerId": 0


    }
    this.unscheduledWorkerList.push(object)
  }
  public formError = false;
  public saveUnscheduledWorkerToStaging() {

    let validateFlag = true;
    let arr = [];
    arr = this.unscheduledWorkerList.map((x) => {
      console.log(x);
      let obj = {
        "ptId": x.personnelTypeId,
        "resourceId": x.resourceId,
        "agencyWorkerName": x.agencyWorker.agencyWorker ? x.agencyWorker.agencyWorker : x.agencyWorker,
        "agencyWorkerId": x.agencyWorkerId ? x.agencyWorkerId : 0,
        "stagingId": x.stagingId == null ? 0 : x.stagingId
      }
      if (x.personnelTypeId == null || x.resourceId == null) {
        validateFlag = false;
        this.formError = true;
      }
      return obj

    })
    let eventDetails = {
      "eventId": this.eventID,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId
    };
    let params = {
      "catererId": this.userdetails.catererId, "eventDetails": eventDetails, "unscheduledWorkersList": arr
    }
    if (validateFlag) {
      try {
        this.caterService.saveUnscheduledWorkerToStaging(params).subscribe(
          res => {
            console.log(res)
            this.getUnscheduledWorkersList();
            let data: any = res
            // alert(data.result);
            swal.fire({
              title: 'success',
              text: data.result,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });

          }
        )
      } catch (error) {

      }
    } else {
      swal.fire('invalid', 'Please fill all highlighted fields before save', 'warning');
    }

  }

  public deleteUnscheduledWorker(i) {

    let flag: boolean = this.unscheduledWorkerList[i].stagingId == null ? false : true;

    swal.fire({
      title: 'Are you sure?',
      text: 'You Want to delete',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        if (flag) {
          try {
            this.caterService.deleteUnscheduledWorker(this.userdetails.catererId, this.unscheduledWorkerList[i].stagingId, this.userdetails.userId, this.userdetails.sessionId).subscribe(
              res => {
                console.log(res);
                swal.fire({
                  title: 'success',
                  text: 'Deleted successfully',
                  icon: 'success',
                  timer: 1200,
                  showConfirmButton: false
                });
                this.getUnscheduledWorkersList();
              }
            )
          } catch (error) {

          }
        } else {
          this.unscheduledWorkerList.splice(i, 1)
        }
      } else if (result.dismiss === swal.DismissReason.cancel) {
        console.log("not deleted")
      }
    })

  }

  public summaryroute() {
    this.router.navigate(['/break-summary-details/' + this.eventID]);

  }
  public breaksummary(id) {
    // this.router.navigate(['/break-summary/' + id]);
    // this.bsmodelRef=this.modalService.show(BreakSummaryComponent,)
    this.bsmodelRef = this.modalService.show(BreakSummaryComponent, {
      class: 'modal-container modal-dialog-centered unschedule-worker-modal-container',
      initialState: {
        id: id,
        data: {}
      }
    })

  }
  public signInFlagUnscheduled(template: TemplateRef<any>, i, flag) {
    this.currentTime = new Date();
    this.popupIndex = i;
    this.popupType = flag;
    this.popupTitle = this.unscheduledWorkerList[i].resourceName;

    let condition = flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut;
    console.log(condition)
    if (!this.pastEventFlag) {
      if (condition == undefined) {
        this.saveToSignInStagingUnscheduled(i, flag, false)
      }
      else {
        if (condition.trim().length > 0) {
          this.popUpminutes = +this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'mm');
          this.popUpHours = +this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'hh');
          this.popUpMeridian = this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'a');
          this.popUpDate = this.datepipe.transform(flag == "signin" ? this.unscheduledWorkerList[i].actCallIn : this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy')
          this.bsmodelRef = this.modalService.show(template, Object.assign({}, { class: 'modal-container modal-dialog-centered' }));

        } else {
          this.saveToSignInStagingUnscheduled(i, flag, false)
        }
      }
    }
  }
  // Save to saveToSignInStaging method  is Used to sign for Scheduled wokers When wokred not passed graceTime;
  public saveToSignInStaging(i, flag, popupFlag) {

    let actualTime = convertDateToMins(this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a'));
    let graceTime = convertDateToMins(this.datepipe.transform(this.eventDetails.eventEndDateTime,'MM/dd/yyyy hh:mm a')) + (this.eventDetails.graceHours *60);

    console.log(actualTime, graceTime,actualTime>graceTime);
    let paramdatepopup = this.datepipe.transform(this.popUpDate, 'MM/dd/yyyy') + " " + this.popUpHours + ":" + this.popUpminutes + " " + this.popUpMeridian;
    let param = {
      "eventId": this.eventID,
      "resourceID": this.rosterData[i].resourceId,
      "scheduleID": this.rosterData[i].scheduleId,
      "scheduledFlag": 1,
      "actualCallIn": flag == "signin" ? popupFlag ? paramdatepopup : actualTime > graceTime && this.eventDetails.graceHours> 0 ? this.rosterData[i].callIn : this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].actCallIn,
      "actualCallOut": flag == "signout" ? popupFlag ? paramdatepopup : actualTime > graceTime && this.eventDetails.graceHours> 0 ? this.rosterData[i].callOut : this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].actCallOut ?? '',
      "break1ActIns": "",
      "break1ActOuts": "",
      "break2ActIns": "",
      "break2ActOuts": "",
      "break3ActIns": "",
      "break3ActOuts": "",
      "personnelTypeId": this.rosterData[i].personnelTypeId,
      "agencyWorkerId": this.agencyWorkerList.some(x => x.agencyWorker === this.rosterData[i].agencyWorker) ? this.rosterData[i].agencyWorkerId : 0,
      "agencyWorker": this.rosterData[i].agencyWorker.agencyWorker ? this.rosterData[i].agencyWorker.agencyWorker : this.rosterData[i].agencyWorker,
      "biometricflag": this.rosterData[i].biometricFlag,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId,
      "catererId": this.userdetails.catererId,
      "stagingId": this.rosterData[i].stagingId

    }
    try {
      this.caterService.saveToSignInStaging(param).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // alert(data.result)
          swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          popupFlag ? this.bsmodelRef.hide() : '';
          this.getRosterData();
          this.getUnscheduledWorkersList();
        },
        (error) => {
          console.log("error came", error)
          swal.fire({
            title: 'Failed',
            text: 'Failed to save',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {
    }
  }






  public saveToSignInStagingUnscheduled(i, flag, popupFlag) {
    console.log(this.unscheduledWorkerList[i].actualCallIn)
    console.log(this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a'))
    let paramdatepopup = this.datepipe.transform(this.popUpDate, 'MM/dd/yyyy') + " " + this.popUpHours + ":" + this.popUpminutes + " " + this.popUpMeridian;
    let param = {
      "eventId": this.eventID,
      "resourceID": this.unscheduledWorkerList[i].resourceId,
      "scheduleID": 0,
      "scheduledFlag": 0,
      "actualCallIn": flag == "signin" ? popupFlag ? paramdatepopup : this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy hh:mm a') == null ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy hh:mm a'),
      "actualCallOut": flag == "signout" ? popupFlag ? paramdatepopup : this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].actCallOut == undefined ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a') == null ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a'),
      "break1ActIns": "",
      "break1ActOuts": "",
      "break2ActIns": "",
      "break2ActOuts": "",
      "break3ActIns": "",
      "break3ActOuts": "",
      "personnelTypeId": this.unscheduledWorkerList[i].personnelTypeId,
      "agencyWorkerId": this.agencyWorkerList.some(x => x.agencyWorker === this.unscheduledWorkerList[i].agencyWorker) ? this.unscheduledWorkerList[i].agencyWorkerId : 0,
      "agencyWorker": this.unscheduledWorkerList[i].agencyWorker.agencyWorker ? this.unscheduledWorkerList[i].agencyWorker.agencyWorker : this.unscheduledWorkerList[i].agencyWorker,
      "biometricflag": 0,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId,
      "catererId": this.userdetails.catererId,
      "stagingId": this.unscheduledWorkerList[i].stagingId

    }
    try {
      this.caterService.saveToSignInStaging(param).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // alert(data.result)
          swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1500,
            showConfirmButton: false
          });
          popupFlag ? this.bsmodelRef.hide() : '';
          this.getUnscheduledWorkersList();
          this.getRosterData();
          this.agencyWorkerList = [];
        },
        (error) => {
          console.log("error came", error)
          swal.fire({
            title: 'Failed',
            text: 'Failed to save',
            icon: 'error',
            // timer: 1200,
            // showConfirmButton: true
          });
        }
      )
    } catch (error) {

    }
  }
  // modal-dialog modal-container modal-dialog-centered unschedule-worker-modal-container

  public break(breaktype, breaknum, i) {
    this.currentTime = new Date();
    let param = {
      "eventId": this.eventID,
      "resourceID": this.rosterData[i].resourceId,
      "scheduleID": this.rosterData[i].scheduleId,
      "scheduledFlag": 1,
      "actualCallIn": this.rosterData[i].actCallIn,
      "actualCallOut": this.rosterData[i].actCallOut,
      "break1ActIns": breaknum == 0 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].break1ActIns ?? '',
      "break1ActOuts": breaknum == 1 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].break1ActOuts ?? '',
      "break2ActIns": breaknum == 1 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].break2ActIns ?? '',
      "break2ActOuts": breaknum == 2 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].break2ActOuts ?? '',
      "break3ActIns": breaknum == 2 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].break3ActIns ?? '',
      "break3ActOuts": breaknum == 3 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.rosterData[i].break3ActOuts ?? '',
      "personnelTypeId": this.rosterData[i].personnelTypeId,
      "agencyWorkerId": this.rosterData[i].agencyWorkerId,
      "agencyWorker": this.rosterData[i].agencyWorker,
      "biometricflag": this.rosterData[i].biometricFlag,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId,
      "catererId": this.userdetails.catererId,
      "stagingId": this.rosterData[i].stagingId

    }

    console.log(param)
    this.saveToSignInStagingFinal(param, true)
  }
  public unschedulebreak(breaktype, breaknum, i) {
    this.currentTime = new Date();
    let param = {
      "eventId": this.eventID,
      "resourceID": this.unscheduledWorkerList[i].resourceId,
      "scheduleID": 0,
      "scheduledFlag": 0,
      "actualCallIn": this.datepipe.transform(this.unscheduledWorkerList[i].actCallIn, 'MM/dd/yyyy hh:mm a'),
      "actualCallOut": this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a') == null ? '' : this.datepipe.transform(this.unscheduledWorkerList[i].actCallOut, 'MM/dd/yyyy hh:mm a'),
      "break1ActIns": breaknum == 0 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break1ActIns !== undefined ? this.unscheduledWorkerList[i].break1ActIns : '',
      "break1ActOuts": breaknum == 1 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break1ActOuts !== undefined ? this.unscheduledWorkerList[i].break1ActOuts : '',
      "break2ActIns": breaknum == 1 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break2ActIns !== undefined ? this.unscheduledWorkerList[i].break2ActIns : '',
      "break2ActOuts": breaknum == 2 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break2ActOuts !== undefined ? this.unscheduledWorkerList[i].break2ActOuts : '',
      "break3ActIns": breaknum == 2 && breaktype == "start" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break3ActIns !== undefined ? this.unscheduledWorkerList[i].break3ActIns : '',
      "break3ActOuts": breaknum == 3 && breaktype == "end" ? this.datepipe.transform(this.currentTime, 'MM/dd/yyyy hh:mm a') : this.unscheduledWorkerList[i].break3ActOuts !== undefined ? this.unscheduledWorkerList[i].break3ActOuts : '',
      "personnelTypeId": this.unscheduledWorkerList[i].personnelTypeId,
      "agencyWorkerId": this.unscheduledWorkerList[i].agencyWorkerId,
      "agencyWorker": this.unscheduledWorkerList[i].agencyWorker,
      "biometricflag": 0,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId,
      "catererId": this.userdetails.catererId,
      "stagingId": this.unscheduledWorkerList[i].stagingId

    }

    console.log(param)
    this.saveToSignInStagingFinal(param, false)
  }
  public noBreak() {
    swal.fire({
      title: 'No Break',
      text: 'Breaks are completed',
      icon: 'warning',
      // timer: 1500,
      showConfirmButton: true
    });
  }


  public saveToSignInStagingFinal(param, flag) {
    try {
      this.caterService.saveToSignInStaging(param).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // alert(data.result)
          swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          flag ? this.getRosterData() : this.getUnscheduledWorkersList();
        },
        (error) => {
          console.log("error came", error)
          swal.fire({
            title: 'Failed',
            text: 'Failed to save',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {
    }
  }

  // two sorting methods
  public sortName(sort1, sort2) {
    this.workersort = !this.workersort
    this.rosterData.sort((a, b) => {
      var x = a.resourceName.toLowerCase();
      var y = b.resourceName.toLowerCase();
      if (x < y) { return sort2; }
      if (x > y) { return sort1; }
      return 0;
    });
    console.log(this.rosterData)
  }
  public sortposition(sort1, sort2) {
    this.positionsort = !this.positionsort
    this.rosterData.sort((a, b) => {
      var x = ((a.positionType) + '-' + (a.sortCode)).toLowerCase();
      var y = ((b.positionType) + '-' + (b.sortCode)).toLowerCase();
      if (x < y) { return sort2; }
      if (x > y) { return sort1; }
      return 0;
    });
    console.log(this.rosterData)
  }
  public sortNameunschedule(sort1, sort2, flag?) {
    this.workersortunschedule = !this.workersortunschedule
    if (flag) {
      this.unscheduledWorkerList.sort((a, b) => {
        var x = a.resourceName.toLowerCase();
        var y = b.resourceName.toLowerCase();
        if (x < y) { return sort2; }
        if (x > y) { return sort1; }
        return 0;
      });
    } else {
      this.unscheduledWorkerList.sort((a, b) => {
        var x = a.resourceName.toLowerCase();
        var y = b.resourceName.toLowerCase();
        if (x < y) { return sort2; }
        if (x > y) { return sort1; }
        return 0;
      });
    }
  }
  public sortpositionunschedule(sort1, sort2, flag?) {
    this.positionsortunschedule = !this.positionsortunschedule;
    if (flag) {
      this.unscheduledWorkerList.sort((a, b) => {
        var x = (a.personnelTypeCode).toLowerCase();
        var y = (b.personnelTypeCode).toLowerCase();
        if (x < y) { return sort2; }
        if (x > y) { return sort1; }
        return 0;
      });
    } else {

      this.unscheduledWorkerList.sort((a, b) => {
        var x = (a.personnelTypeCode).toLowerCase();
        var y = (b.personnelTypeCode).toLowerCase();
        if (x < y) { return sort2; }
        if (x > y) { return sort1; }
        return 0;
      });
    }
  }




  public getAgencyWorkers(resourcId, index, flag) {

    console.log(this.agencyWorkerList)
    try {
      this.caterService.getAgencyWorkers(this.userdetails.catererId, resourcId).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.agencyWorkerList = JSON.parse(JSON.stringify(data.agencyWorkerList));
          let idname = flag + index;
          document.getElementById(idname).click();


          this.agencyWokerListIndex = index;
        }
      )
    } catch (error) {

    }
  }
  onSelect(event: TypeaheadMatch, i, flag?): void {
    console.log("eventt", event)
    if (flag == 'unschedule') {
      this.unscheduledWorkerList[i].agencyWorker = event.item.agencyWorker;
      this.unscheduledWorkerList[i].agencyWorkerId = event.item.agencyWorkerId;
    } else {
      this.rosterData[i].agencyWorker = event.item.agencyWorker;
      this.rosterData[i].agencyWorkerId = event.item.agencyWorkerId;
    }

    console.log(this.unscheduledWorkerList)
  }

  public onUnscheduleClose() {

    let arr = this.unscheduledWorkerList.filter(x => x.stagingId == null);
    console.log(arr);
    if (arr.length > 0) {
      swal.fire({
        title: 'Are you sure you want to Discard changes?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Discard',
      }).then((result) => {
        if (result.isConfirmed) {
          $('#Unscheduled').modal('hide')

        }
      })
    }
    else {
      $('#Unscheduled').modal('hide')

    }

  }

  public approveUnscheduledWorker(item) {
    try {
      let obj =
      {
        "stagingId": item.stagingId,
        "catererId": this.userdetails.catererId,
        "userId": this.userdetails.userId
      }
      this.caterService.approveUnscheduledWorker(obj).subscribe(res=>{
        console.log(res);
        if(res.sucess==1){
          swal.fire('Success','Worker approved successfully','success');
          this.getUnscheduledWorkersList();
        }
      })


    } catch (error) {

    }
  }
}

interface DataSourceType {
  agencyWorkerId: number;
  agencyWorker: string;
}



// export function convertHoursToMins(time) {
//   var hours = Number(time.match(/^(\d+)/)[1]);
//   var minutes = Number(time.match(/:(\d+)/)[1]);
//   var AMPM = time.match(/\s(.*)$/)[1];
//   if (AMPM == "PM" && hours < 12) hours = hours + 12;
//   if (AMPM == "AM" && hours == 12) hours = hours - 12;
//   var sHours = hours.toString();
//   var sMinutes = minutes.toString();
//   if (hours < 10) sHours = "0" + sHours;
//   if (minutes < 10) sMinutes = "0" + sMinutes;
//   let totalminutes = +sHours * 60;

//   return Number(totalminutes + (+sMinutes));
// }
export function convertDateToMins(dateTime) {

let seconds=Date.parse(dateTime);

let minutes=seconds/60000;

  return Number(minutes)
}


