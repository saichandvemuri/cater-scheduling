import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakDetailsComponent } from './break-details.component';

describe('BreakDetailsComponent', () => {
  let component: BreakDetailsComponent;
  let fixture: ComponentFixture<BreakDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreakDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
