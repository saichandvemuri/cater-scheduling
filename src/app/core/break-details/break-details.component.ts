import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { HoursDifference } from 'src/app/pipes/domsanitize.pipe';
import { CaterService } from 'src/app/services/cater.service';
import Swal from 'sweetalert2';
import swal from 'sweetalert2';

@Component({
  selector: 'app-break-details',
  templateUrl: './break-details.component.html',
  styleUrls: ['./break-details.component.scss']
})
export class BreakDetailsComponent implements OnInit {

  public userdetails: any;
  public deviceId: any;
  public workerName = null;
  public eventID;
  public positionTypeId = null;
  public personnelTypesList: Array<any> = [];
  public bsmodelRef: BsModalRef;
  public minutesArray: Array<number> = new Array<number>(60);
  public positionsort: boolean = true;
  public workersort: boolean = true;
  public popUpminutes = null;
  public popUpHours = null;
  public popUpMeridian = null;
  public popUpDate;
  public popupIndex: number;
  public popupType: string;
  public popupTitle: string;
  public eventDetails: any;
  public summaryData: Array<any> = [];
  public pastEventFlag: boolean = false;
  public agencyWorkerList: Array<any> = [];
  public agencyWokerListIndex;
  public agencyWorkerLabel = 'agencyWorker';
  public popupTypeTitle='';
  public eventDate=''
  constructor(public caterService: CaterService,public hoursDiffernce:HoursDifference ,public datepipe: DatePipe, private activatedRoute: ActivatedRoute, private modalService: BsModalService, private router: Router) {
    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'));
    this.deviceId = JSON.parse(sessionStorage.getItem('ipAdd'));
    this.eventDetails = JSON.parse(sessionStorage.getItem('eventdetails'));
    // this.eventDate = this.eventDetails.nonEventFlag == 1 ? this.datepipe.transform(this.eventDetails.eventDate, 'MM/dd/yyyy') : '';

    this.eventID = this.activatedRoute.snapshot.params.eventId;
    console.log(this.eventID)
    let eventDate = Date.parse(this.eventDetails.eventDate);
    let systemdate = Date.parse(this.datepipe.transform(new Date(), 'MM/dd/yyyy'))
    console.log(eventDate, systemdate, eventDate > systemdate)
    if ((this.eventDetails.enableSubmitFlag ==1)) {
      this.pastEventFlag = false;
    } else {
      this.pastEventFlag = true;
    }
  }

  ngOnInit(): void {
    this.getSummaryList();
  }

  public getSummaryList() {
     let date=this.datepipe.transform(this.eventDate, 'MM/dd/yyyy');
    try {
      this.caterService.getSummaryList(this.userdetails.catererId, this.eventID, this.userdetails.sessionId, this.workerName == null ? '' : this.workerName, this.positionTypeId == null ? '' : this.positionTypeId,this.eventDetails.nonEventFlag == 1 &&date!=null ? this.datepipe.transform(this.eventDate, 'MM/dd/yyyy') : '').subscribe(
        res => {
          console.log(res)
          let data: any = res;
          this.summaryData = data.summaryData;
          this.personnelTypesList = data.personnelTypesList;
          this.summaryData.map(x=>{
            if(x.actCallOut.length>0&& x.actCallIn.length>0){
              let differnceMinutes=this.hoursDiffernce.transform(this.datepipe.transform(x.actCallIn,'MM/dd/yyyy h:mm a'),this.datepipe.transform(x.actCallOut,'MM/dd/yyyy h:mm a'));
              x.totalHours= (differnceMinutes/60).toFixed(2);
            }else {
              x.totalHours='';
            }
          })
        }
      )
    } catch (error) {

    }


  }

  public saveSummaryList() {
    let unApproved=this.summaryData.filter(x=>x.approveFlag==0&&x.scheduledFlag==0)
    console.log(unApproved)
    let object = {
      "eventId": this.eventID,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId
    }
    let arr =this.summaryData.filter(x=>x.approveFlag!=0||x.scheduledFlag==1).map((x) => {
      let sobj = {
        "resourceId": x.resourceId,
        "scheduleId": x.scheduleId,
        "scheduledFlag": x.scheduledFlag,
        "actualCallIn": x.actCallIn,
        "actualCallOut": x.actCallOut,
        "personnelTypeId": x.personnelTypeId,
        "agencyWorker": x.agencyWorker,
        "break1ActIns": x.break1ActIn,
        "break1ActOuts": x.break1ActOut,
        "break2ActIns": x.break2ActIn,
        "break2ActOuts": x.break2ActOut,
        "break3ActIns": x.break3ActIn,
        "break3ActOuts": x.break3ActOut,
        "appPostScheduleIds": 1,
        "agencyWorkerIds": "",
        "biometricFlag": x.biometricFlag,
        "stagingId": x.stagingId

      }
      return sobj;
    })
;
   if(unApproved.length>0){
    swal.fire({
      title: 'There are unapproved unscheduled workers exists in the event. Do you still want to continue?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Submit',
    }).then((result) => {
      if (result.isConfirmed) {

 let parametrs = { "catererId": this.userdetails.catererId, "eventDetails": object, "summaryList": arr }
    try {
      this.caterService.saveSummaryList(parametrs).subscribe(
        res => {
          console.log(res);
          swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          this.router.navigateByUrl('events')

        },
        error => {
          swal.fire({
            title: 'Error',
            text: 'Failed to save',
            icon: 'error',
            timer: 1200,
            showConfirmButton: false
          });
        }
      )
    } catch (error) {

    }

      }
    })
   }else{
    let parametrs = { "catererId": this.userdetails.catererId, "eventDetails": object, "summaryList": arr }
    try {
      this.caterService.saveSummaryList(parametrs).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // this.getSummaryList();
          swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          this.router.navigateByUrl('events')

        },
        error => {
          swal.fire({
            title: 'Error',
            text: 'Failed to save',
            icon: 'error',
            timer: 1200,
            showConfirmButton: false
          });
        }
      )
    } catch (error) {

    }
   }



  }
  public signInFlag(template: TemplateRef<any>, i, flag, date) {
    this.popupIndex = i;
    this.popupType=flag;
    if (flag == 'break1in') {
      this.popupTypeTitle = 'Break1 In';
    } else if (flag == 'break2in') {
      this.popupTypeTitle = 'Break2 In';
    } else if (flag == 'break3in') {
      this.popupTypeTitle = 'Break3 In';
    } else if ( flag == 'break1out') {
      this.popupTypeTitle = 'Break1 Out';
    }else if (flag ==  'break2out') {
      this.popupTypeTitle = 'Break2 Out';
    }else if (flag == "break3out") {
      this.popupTypeTitle = 'Break3 Out';
    } else if (flag == 'signin') {
      this.popupTypeTitle = 'Sign In'
    } else if (flag == 'signout') {
      this.popupTypeTitle = 'Sign Out'
    }
    this.popupTitle = this.summaryData[i].resourceName;

    let condition = date;
    console.log(condition)
    console.log(condition.length)
    if (!this.pastEventFlag) {
      if (condition == undefined || condition.length == 0) {
        let currentdate = new Date()
        // this.saveToSignInStaging(i, flag, false)
        this.popUpminutes = +this.datepipe.transform(currentdate, 'mm');
        this.popUpHours = +this.datepipe.transform(currentdate, 'hh');
        this.popUpMeridian = this.datepipe.transform(currentdate, 'a');
        this.popUpDate = this.datepipe.transform(currentdate, 'MM/dd/yyyy')
        this.bsmodelRef = this.modalService.show(template, Object.assign({
          backdrop: false,
          animated: true,
          keyboard: false,
          ignoreBackdropClick: true,
        }, { class: 'modal-container modal-dialog-centered' }));

      }
      else {
        if (condition.trim().length > 0) {
          this.popUpminutes = +this.datepipe.transform(date, 'mm');
          this.popUpHours = +this.datepipe.transform(date, 'hh');
          this.popUpMeridian = this.datepipe.transform(date, 'a');
          this.popUpDate = this.datepipe.transform(date, 'MM/dd/yyyy')
          this.bsmodelRef = this.modalService.show(template, Object.assign({
            backdrop: false,
            animated: true,
            keyboard: false,
            ignoreBackdropClick: true,
          }, { class: 'modal-container modal-dialog-centered' }));

        }
      }
    }

  }

  public savePopup(index, popupflag) {
    this.bsmodelRef.hide();
    let paramdatepopup = this.popUpDate + " " + this.popUpHours + ":" + this.popUpminutes + " " + this.popUpMeridian;
    if (popupflag == 'signin') {
      this.summaryData[index].actCallIn = paramdatepopup;
    } else if (popupflag == "signout") {

    }

  }
  public saveToSignInStaging(i, flag, popupFlag) {
    console.log(popupFlag)
    let validateFlag=true;
    let paramdatepopup = this.datepipe.transform(this.popUpDate, 'MM/dd/yyyy') + " " + this.popUpHours + ":" + this.popUpminutes + " " + this.popUpMeridian;
    if(flag=='signin'||flag=='signout'){
      let startTime=flag=='signin'?paramdatepopup:this.summaryData[i].actCallIn;
      let endTime=flag=='signout'?paramdatepopup:this.summaryData[i].actCallOut;
      console.log(startTime,endTime);
      if(Date.parse(startTime)>Date.parse(endTime)){
        validateFlag=false;
        Swal.fire('Warning','SignIn time cannot be greater than SignOut time','warning');
      }



    }
    if(validateFlag){

    let param = {
      "eventId": this.eventID,
      "resourceID": this.summaryData[i].resourceId,
      "scheduleID": this.summaryData[i].scheduleId,
      "scheduledFlag": this.summaryData[i].scheduledFlag,
      "actualCallIn": flag == "signin" ? paramdatepopup : this.summaryData[i].actCallIn,
      "actualCallOut": flag == "signout" ? paramdatepopup : this.summaryData[i].actCallOut,
      "break1ActIns": flag == "break1in" ? paramdatepopup : this.summaryData[i].break1ActIn,
      "break1ActOuts": flag == "break1out" ? paramdatepopup : this.summaryData[i].break1ActOut,
      "break2ActIns": flag == "break2in" ? paramdatepopup : this.summaryData[i].break2ActIn,
      "break2ActOuts": flag == "break2out" ? paramdatepopup : this.summaryData[i].break2ActOut,
      "break3ActIns": flag == "break3in" ? paramdatepopup : this.summaryData[i].break3ActIn,
      "break3ActOuts": flag == "break3out" ? paramdatepopup : this.summaryData[i].break3ActOut,
      "personnelTypeId": this.summaryData[i].personnelTypeId,
      "agencyWorkerId": this.agencyWorkerList.some(x => x.agencyWorker === this.summaryData[i].agencyWorker) ? this.summaryData[i].agencyWorkerId : 0,
      "agencyWorker": this.summaryData[i].agencyWorker.agencyWorker ? this.summaryData[i].agencyWorker.agencyWorker : this.summaryData[i].agencyWorker,
      "biometricflag": this.summaryData[i].biometricFlag,
      "userId": this.userdetails.userId,
      "deviceId": this.deviceId.ip,
      "platform": "WebSignInAPP",
      "sessionId": this.userdetails.sessionId,
      "catererId": this.userdetails.catererId,
      "stagingId": this.summaryData[i].stagingId

    }
    try {
      this.caterService.saveToSignInStaging(param).subscribe(
        res => {
          console.log(res);
          let data: any = res
          // alert(data.result)
          popupFlag ? this.bsmodelRef.hide() : '';
          swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          this.getSummaryList();

        },
        (error) => {
          console.log("error came", error)
          swal.fire({
            title: 'Failed',
            text: 'Failed to save',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {
    }
  }
  }
  public roster() {
    this.router.navigate(['/roster/' + this.eventID]);

  }

  public sortName(sort1, sort2) {
    this.workersort = !this.workersort
    this.summaryData.sort((a, b) => {
      var x = a.resourceName.toLowerCase();
      var y = b.resourceName.toLowerCase();
      if (x < y) { return sort2; }
      if (x > y) { return sort1; }
      return 0;
    });
    console.log(this.summaryData)
  }
  public sortposition(sort1, sort2) {
    this.positionsort = !this.positionsort
    this.summaryData.sort((a, b) => {
      var x = ((a.positionType) + '-' + (a.sortCode)).toLowerCase();
      var y = ((b.positionType) + '-' + (b.sortCode)).toLowerCase();
      if (x < y) { return sort2; }
      if (x > y) { return sort1; }
      return 0;
    });
    console.log(this.summaryData)
  }

  public getAgencyWorkers(resourcId, index, flag) {
    try {
      this.caterService.getAgencyWorkers(this.userdetails.catererId, resourcId).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.agencyWorkerList = data.agencyWorkerList;
          this.agencyWokerListIndex = index;
          document.getElementById(flag + index).click();

        }
      )
    } catch (error) {

    }
  }

  public onSelect(event: TypeaheadMatch, i): void {
    console.log("eventt", event)
    this.summaryData[i].agencyWorker = event.item.agencyWorker;
    this.summaryData[i].agencyWorkerId = event.item.agencyWorkerId;
    console.log(this.summaryData)
  }
  public approveUnscheduledWorker(item) {
    try {
      let obj =
      {
        "stagingId": item.stagingId,
        "catererId": this.userdetails.catererId,
        "userId": this.userdetails.userId
      }
      this.caterService.approveUnscheduledWorker(obj).subscribe(res=>{
        console.log(res);
        if(res.sucess==1){
          swal.fire('Success','Worker approved successfully','success');
          this.getSummaryList();

        }
      })


    } catch (error) {

    }
  }
}
