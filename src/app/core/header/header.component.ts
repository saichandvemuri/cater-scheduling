import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CaterService } from 'src/app/services/cater.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public userdetails: any;
  constructor(private caterService: CaterService,private router:Router,public devicedetector:DeviceDetectorService) {
    this.userdetails = JSON.parse(sessionStorage.getItem('userdetails'))
    console.log(devicedetector.isDesktop(),devicedetector.isMobile())
  }

  ngOnInit(): void {
  }
  logOutUser() {
    Swal.fire({
      title: 'Are you sure you want to Logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Logout!'
    }).then((result) => {
      if (result.isConfirmed) {
      try {
      this.caterService.logOutUser(this.userdetails.catererId, this.userdetails.sessionId).subscribe(
        res => {
          console.log(res)
          setTimeout(() => {
            this.router.navigateByUrl('login');
            sessionStorage.removeItem('userdetails');
            sessionStorage.removeItem('eventFilterDetails');
            sessionStorage.removeItem('eventdetails');

          }, 100);
        }
      )
    } catch (error) {

    }
  }
})
  }
public home(){
  if(this.devicedetector.isMobile()){
    this.router.navigateByUrl('event');

  }else{
    console.log(this.devicedetector.isDesktop());
    this.router.navigateByUrl('events');

  }
}
}
