import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BreakDetailsComponent } from './core/break-details/break-details.component';
import { BreakSummaryComponent } from './core/break-summary/break-summary.component';
import { HeaderComponent } from './core/header/header.component';
import { ListingComponent } from './core/listing/listing.component';
import { LoginComponent } from './core/login/login.component';
import { BreaksummaryMobileComponent } from './core/mobile screens/breaksummary-mobile/breaksummary-mobile.component';
import { EventComponent } from './core/mobile screens/event/event.component';
import { QrcodeComponent } from './core/mobile screens/qrcode/qrcode.component';
import { RoasterComponent } from './core/mobile screens/roaster/roaster.component';
import { SummaryMobileComponent } from './core/mobile screens/summary-mobile/summary-mobile.component';
import { UnscheduledMobileComponent } from './core/mobile screens/unscheduled-mobile/unscheduled-mobile.component';
import { RosterEventComponent } from './core/roster-event/roster-event.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: "/login", pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'events',canActivate:[AuthGuard], component: ListingComponent },
  { path: 'roster/:eventId',canActivate:[AuthGuard], component: RosterEventComponent },
  { path: 'break-summary/:stagingId',canActivate:[AuthGuard], component: BreakSummaryComponent },
  { path: 'break-summary-details/:eventId',canActivate:[AuthGuard], component: BreakDetailsComponent },
  { path: 'header',canActivate:[AuthGuard], component: HeaderComponent },
  { path: 'event', component: EventComponent },
  { path: 'roaster/:eventId',canActivate:[AuthGuard], component: RoasterComponent },
  { path: 'roaster',canActivate:[AuthGuard], component: RoasterComponent },
  { path: 'msummary-details/:eventId',canActivate:[AuthGuard], component: SummaryMobileComponent },
  { path: 'mbreak-summary/:eventId',canActivate:[AuthGuard], component: BreaksummaryMobileComponent },
  { path: 'munscheduled/:eventId',canActivate:[AuthGuard], component: UnscheduledMobileComponent },
  { path: 'qrc/:eventId/:eventDate',canActivate:[AuthGuard], component: QrcodeComponent },
  { path: 'qrc/:eventId',canActivate:[AuthGuard], component: QrcodeComponent },
  {path:'**',redirectTo:'/events'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
